#!/bin/bash

cat urls.txt | while read line
do
 echo $line
 pageres 320x480 768x640 1024x768 $line --filename="<%= date %> - <%= time %> - <%= url %>-<%= size %><%= crop %>"
done
