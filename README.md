# README #

# Intro #

bat/sh script to pageres
Быстро делает скриншоты по заданным страницам и в заданных разрешениях на webkit (phantomjs)


# Required #

1. nodejs > 0.12
Проверить версию в консоле node -v

2. https://github.com/sindresorhus/pageres
3. https://github.com/sindresorhus/pageres-cli
4. http://phantomjs.org/ - установится сам при установке pageres

# Install #

Win:

1. Установить nodejs
https://nodejs.org/en/
2. Установить pageres.
npm install pageres -g
3. Установить pageres.
npm install pageres-cli -g
4. Распаковать файлы из /win в отдельную папку.
5. В файле start.bat поправить путь до файлу urls.txt

Nix (debian/ubuntu/mint...):

1. Установить nodejs
sudo apt-get install nodejs-legacy
2.  Установить npm
sudo apt-get install npm
3. Установить pageres.
npm install pageres -g
4. Установить pageres.
npm install pageres-cli -g
5. Распаковать файлы из /nix в отдельную папку.
6. Установить права на выполнения для start.sh

# Use #

1. В файл urls.txt записываем адреса страниц. Один на строку.
2. В файле start.bat/sh задаем разрешения в которых делать скриншоты.
3. Запускаем start.bat/sh
4. Скриншоты появятся в папке.

Если есть проблемы, то читаем https://github.com/sindresorhus/pageres и https://github.com/sindresorhus/pageres-cli